package gemad.bg.translate.data;

public class ResponseCodes {

    public static String codeMeaning(int code){
        switch (code){
            case 200: return "Operation completed successfully";
            case 401: return "Invalid API key";
            case 402: return "Blocked API key";
            case 404: return "Exceeded the daily limit on the amount of translated text";
            case 413: return "Exceeded the maximum text size";
            case 422: return "The text cannot be translated";
            case 501: return "The specified translation direction is not supported";
            default: return "Unknown response!";
        }
    }
}
