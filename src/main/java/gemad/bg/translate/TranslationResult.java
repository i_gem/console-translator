package gemad.bg.translate;

import gemad.bg.translate.data.ResponseCodes;

//contains translation result from yandex translate
public class TranslationResult {

    public int code;
    public String lang;
    public String[] text;

    public int getCode() {
        return code;
    }

    public String[] getText() {
        return text;
    }

    //returns message corresponding to the response code
    public String responseType() {
        return ResponseCodes.codeMeaning(code);
    }
}
