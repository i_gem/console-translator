package gemad.bg.translate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Starter {
    public static final String KEY_LOCATION = System.getProperty("user.dir") + File.separator + "key.txt";
    public static void main(String[] args) {
        String key = "";
        try {
            key = readKey(KEY_LOCATION);
        } catch (IOException e) {
            System.out.println("Unable to find a key.txt file");
            return;
        }
        Terminal terminal = new Terminal(key);
        terminal.go();
    }

    //reads API key from file and returns it
    public static String readKey(String filePath) throws IOException {
        String key;
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            key = br.readLine();
        }
        return key;
    }
}
