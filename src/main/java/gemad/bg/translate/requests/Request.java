package gemad.bg.translate.requests;

import java.net.URL;

public interface Request {

    String getType();
    String getParameters();
    URL getURL();

}
