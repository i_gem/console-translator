package gemad.bg.translate.requests;


import java.net.MalformedURLException;
import java.net.URL;

public class GetRequest implements Request {
    private String url;
    private String parameters;

    public GetRequest(String url, String parameters){
        this.url = url;
        this.parameters = parameters;
    }


    public String getType() {
        return "GET";
    }

    public String getParameters() {
        return parameters;
    }

    public URL getURL() {
        try {
            return new URL(url + "?" + parameters);
        } catch (MalformedURLException e) {
            System.out.println("Incorrect URL format.");
            return null;
        }

    }
}
