package gemad.bg.translate.requests;

import java.net.MalformedURLException;
import java.net.URL;

public class PostRequest implements Request {
    private String url;
    private String parameters;

    public PostRequest(String url, String parameters){
        this.url = url;
        this.parameters = parameters;
    }


    public String getType() {
        return "POST";
    }

    public String getParameters() {
        return parameters;
    }

    public URL getURL() {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            System.out.println("Incorrect URL format");
            return null;
        }
    }
}
