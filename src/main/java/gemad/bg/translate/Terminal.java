package gemad.bg.translate;

import com.google.gson.Gson;
import gemad.bg.translate.data.Parameters;
import gemad.bg.translate.requests.PostRequest;
import gemad.bg.translate.requests.Request;

import java.io.*;
import java.net.URLEncoder;
import java.util.Scanner;


public class Terminal {

    private String key;//API key from txt file

    public Terminal(String key){
        this.key = key;
    }
    //takes input from console and uses translate() method to send request, then outputs result
    public void go() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Powered by Yandex.Translate - http://translate.yandex.com/\n");
        while (true) {
            if (scanner.hasNext()) {
                String text = scanner.nextLine();
                if (validText(text)) {
                    String result = translate(text);
                    if (result != null)
                        System.out.println(result);
                    else
                        System.out.println("Unable to translate");
                } else {
                    if (!text.isEmpty()) //scanner can stack empty lines
                        System.out.println("Input text must be from 1 to 10000 symbols long");
                }
            }
        }
    }

    public static boolean validText(String text) {
        return !(text == null || text.isEmpty() || text.length() > 10000);
    }

    //returns translated text or null
    public String translate(String text) {
        String result = null;
        try {
            String encoded = URLEncoder.encode(text, "UTF-8"); //encoding text in URL format
            Request request = new PostRequest(Parameters.URL, key + "&" + Parameters.LANGUAGE + "&text=" + encoded);
            TranslationResult tr1;
            HttpsClient client = new HttpsClient(); //creating https client
            client.setRequest(request); //sending request
            Gson gson = new Gson();
            String response = client.sendRequest(); //getting response from server
            if (response != null)
                tr1 = gson.fromJson(client.sendRequest(), TranslationResult.class); //parsing json in TR object
            else return null;
            result = tr1.getText()[0]; //getting translated text
        } catch (UnsupportedEncodingException e) {
            System.out.println("Encoding error. Program supports only UTF-8 encoding");
        }
        return result;
    }



}
