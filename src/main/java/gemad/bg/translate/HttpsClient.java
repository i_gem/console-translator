package gemad.bg.translate;


import gemad.bg.translate.data.ResponseCodes;
import gemad.bg.translate.requests.Request;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static gemad.bg.translate.Util.streamToString;

public class HttpsClient {
    private URL url;
    private Request request;

    public HttpsClient() {

    }

    public void setRequest(Request request){
        this.request = request;
    }

    //sends GET or POST request
    public String sendRequest(){
        HttpsURLConnection con;
        InputStream content;

        try {
            con = (HttpsURLConnection) request.getURL().openConnection();
            con.setRequestMethod(request.getType());
            if ("POST".equals(request.getType())){
                con.setDoOutput(true);
                try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                    wr.writeBytes(request.getParameters());
                }
            }
            int code = con.getResponseCode();
            if (code == 200) {
                content = (InputStream) con.getContent();
                return Util.streamToString(content);
            } else {
                System.out.println("Response code " + code + ": " + ResponseCodes.codeMeaning(code));
            }
        } catch (IOException e) {
            System.out.println("Connection cannot be established.");
        }

        return null;
    }

    public void setUrl(String s) throws MalformedURLException {
        url = new URL(s);
    }
}
