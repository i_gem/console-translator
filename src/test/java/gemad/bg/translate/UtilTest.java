package gemad.bg.translate;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class UtilTest {

    @Test
    public void streamToStringTest(){
        String string1 = "";
        String string2 = "test string";
        char[] c = new char[20000];
        for (int i = 0; i < 10000; i++) {
            c[i] = 'a';
            c[i + 10000] = 'b';
        }
        String longString = new String(c);
        try (InputStream targetStream = new ByteArrayInputStream(string1.getBytes())) {
            assertEquals(string1, Util.streamToString(targetStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStream targetStream = new ByteArrayInputStream(string2.getBytes())) {
            assertEquals(string2, Util.streamToString(targetStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (InputStream targetStream = new ByteArrayInputStream(longString.getBytes())) {
            assertEquals(longString, Util.streamToString(targetStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
