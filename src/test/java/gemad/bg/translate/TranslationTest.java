package gemad.bg.translate;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

//these tests might fail, depends on translation results
public class TranslationTest {
    @Test
    public void translation() {
        char[] longArr = new char[10000];
        for (int i = 0; i < longArr.length; i++) {
            longArr[i] = 'a';
        }
        String line = new String(longArr);
        String key = null;
        try {
            key = Starter.readKey(Starter.KEY_LOCATION);
            Terminal terminal = new Terminal(key);
            assertEquals(terminal.translate(line).length(), line.length());

            String severalLines = "aaaaaa\n\naaaaaa\n\naaaaaa";
            assertEquals(terminal.translate(severalLines).length(), severalLines.length());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
