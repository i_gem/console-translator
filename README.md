This is a simple console program that lets you translate phrases from english to russian language. 

You need to put a file named "key.txt" in the same folder with a program. Since this program uses Yandex.Translate for translation, Yandex.Translate API key must be inside this file in the following format:

key=<your API key>

< and > brackets are not part of the line.


Это простая консольная программа, которая позволяет переводить фразы с английского на русский язык.

Вам нужно поместить файл с именем «key.txt» в одну папку с программой. Поскольку для перевода в этой программе используется Яндекс.Переводчик, ключ API Яндекс.Переводчика должен находиться внутри этого файла в следующем формате:

ключ=<ваш ключ API>

Скобки < и > не являются частью строки.


Powered by Yandex.Translate - http://translate.yandex.com/